Game starts with player 1
Player1 moves with UP,DOWN,LEFT,RIGHT arrow keys
Player2 moves with W,A,S,D keys
Both players have 3 lives
Each player gains points for crossing obstacles and extra score inversely proprtional to the time it takes for him to cross the river
Objective is to cross the river
Player starts at the beginning of the level after dying
Extra points will not be awarded for recrossing obstacles if player dies mid-river 
Time does not get reset if you die either
Speed of objects increases after every level
Level increases when both players have played that level or if one player is dead and second player crosses the river
Game ends when both players have exhausted all their lives
