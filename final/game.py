import random
import pygame
from config import *
pygame.init()

# Loading images

triangle = pygame.image.load('./player.png')
opptri = pygame.image.load('./player2.png')
global imm
imm = triangle
global shark
shark = pygame.image.load('./shark.png')
global rock
rock = pygame.image.load('./rock.png')
global use
use = shark

# Declaring velocities and current player

which_p = 1
level = 1
global v
global k
k = []
v1 = []
v2 = []
for i in range(9):
    v1.append(random.randint(1, 4))
for i in range(9):
    v2.append(v1[i])


# Creating class for players

class Player(object):

    def __init__(
        self,
        x,
        y,
        width,
        height,
    ):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.ve = 8
        self.hitbox = (self.x, self.y, self.width, self.height)
        self.health = 3
        self.ix = x
        self.iy = y
        self.score = 0

# Draw the player

    def draw(self, win):
        win.blit(imm, (self.x, self.y))

# To draw hitbox if required
#       pygame.draw.rect(win,(0,0,255),self.hitbox,2)

        self.hitbox = (self.x, self.y, self.width, self.height)

# If player gets hit

    def hit(self):
        pygame.time.delay(100)
        self.health = self.health - 1
        self.x = self.ix
        self.y = self.iy


# Class for obstacles

class Obstacle(object):

    def __init__(
        self,
        x,
        y,
        width,
        height,
        end,
    ):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.end = end
        self.ve = 3
        self.path = [self.x, self.end]
        self.hitbox = (self.x + 20, self.y, 28, 60)
        self.op = use

# Draw and move the obstacles

    def draw(self, win):
        self.move()
        win.blit(self.op, (self.x, self.y))

# To draw hitbox if required
#        pygame.draw.rect(win,(0,0,255),self.hitbox,2)

        self.hitbox = (self.x, self.y, self.width, self.height)

    def move(self):
        self.x = (self.x + self.ve) % 1080


# Function to load everything onto the screen

def refresh():

    # Drawing the background

    win.fill((59, 179, 208))
    pygame.draw.rect(win, (150, 75, 0), (0, 0, 1080, 55))
    pygame.draw.rect(win, (150, 75, 0), (0, 110, 1080, 55))
    pygame.draw.rect(win, (150, 75, 0), (0, 220, 1080, 55))
    pygame.draw.rect(win, (150, 75, 0), (0, 330, 1080, 55))
    pygame.draw.rect(win, (150, 75, 0), (0, 440, 1080, 55))
    pygame.draw.rect(win, (150, 75, 0), (0, 440, 1080, 55))
    pygame.draw.rect(win, (150, 75, 0), (0, 550, 1080, 55))
    pygame.draw.rect(win, (150, 75, 0), (0, 660, 1080, 80))

    # When both exhaust their lives

    if p1.health <= 0 and p2.health <= 0:
        win.fill((0, 0, 0))
        ans = font.render('Score of player1: ' + str(p1.score), 1,
                          (255, 255, 255))
        ans2 = font.render('Score of player2: ' + str(p2.score), 1,
                           (255, 255, 255))
        win.blit(ans, (500, 345))
        win.blit(ans2, (500, 365))
        if p1.score > p2.score:
            sol = font.render('Player1 wins', 1, (255, 255, 255))
            win.blit(sol, (510, 385))
        elif p2.score > p1.score:
            sol = font.render('Player2 wins', 1, (255, 255, 255))
            win.blit(sol, (510, 385))
        else:
            sol = font.render('Tie', 1, (255, 255, 255))
            win.blit(sol, (510, 385))
        pygame.display.update()
        pygame.time.delay(5000)
        pygame.quit()
        exit()

    # Displaying statistics

    if p1.health > 0:
        text1 = font.render('Player1 lives: ' + str(p1.health), 1, (0,
                                                                    0, 0))
    elif p1.health <= 0:
        text1 = font.render('Player1 has lost', 1, (0, 0, 0))
    if p2.health > 0:
        text2 = font.render('Player2 lives: ' + str(p2.health), 1, (0,
                                                                    0, 0))
    elif p2.health <= 0:
        text2 = font.render('Player2 has lost', 1, (0, 0, 0))
    text = font.render('Level: ' + str(level), 1, (0, 0, 0))
    win.blit(text1, (10, 0))
    win.blit(text2, (900, 0))
    win.blit(text, (520, 0))
    sc1 = font.render('Player1 score: ' + str(p1.score), 1, (0, 0, 0))
    sc2 = font.render('Player2 score: ' + str(p2.score), 1, (0, 0, 0))
    win.blit(sc1, (10, 20))
    win.blit(sc2, (900, 20))
    te = font.render('Player ' + str(which_p) + ' is playing', 1, (0,
                                                                   0, 0))

    # Displaying players and obstacles

    for i in range(17):
        k[i].draw(win)
    if which_p == 1:
        p1.draw(win)
    elif which_p == 2:
        p2.draw(win)
    win.blit(te, (10, 700))
    pygame.display.update()


# Flag for crash condition

global cra
cra = False

# Array to check achievements

ispass = [0]
ispass2 = [0]
for i in range(11):
    ispass.append(0)
    ispass2.append(0)


# Function to check for crash between player and single obstacle

def crash(p1, e1):
    if p1.hitbox[1] > e1.hitbox[1] and p1.hitbox[1] < e1.hitbox[1] \
        + e1.hitbox[3] or p1.hitbox[1] + p1.hitbox[3] < e1.hitbox[1] \
        + e1.hitbox[3] and p1.hitbox[1] + p1.hitbox[3] > e1.hitbox[1] \
        or p1.hitbox[1] < e1.hitbox[1] and p1.hitbox[1] + p1.hitbox[3] \
            > e1.hitbox[1] + e1.hitbox[3]:
        if p1.hitbox[0] > e1.hitbox[0] and p1.hitbox[0] < e1.hitbox[0] \
            + e1.hitbox[2] or p1.hitbox[0] + p1.hitbox[2] \
            < e1.hitbox[0] + e1.hitbox[2] and p1.hitbox[0] \
            + p1.hitbox[2] > e1.hitbox[0] or p1.hitbox[0] \
            < e1.hitbox[0] and p1.hitbox[0] + p1.hitbox[2] \
                > e1.hitbox[0] + e1.hitbox[2]:
            return True
    return False


# Setting poition of obstacles

for i in range(17):
    e = Obstacle(0, 0, 30, 30, 1050)
    k.append(e)
k[0].y = 620
k[1].y = 500
k[2].y = 500
k[3].y = 400
k[4].y = 400
k[5].y = 280
k[6].y = 180
k[7].y = 180
k[8].y = 70
k[9].y = 570

# Setting velocities of obstacles

for i in range(9):
    k[i].ve = random.randint(1, 4)
global ee
ee = []
for i in range(9):
    ee.append(0)


def ran():
    for i in range(9):
        ee[i] = random.randint(2, 4)


ran()

# Setting stationary obstacles

for i in range(8):
    k[i + 9].y = k[i].y - 50
    k[i + 9].ve = 0
    k[i + 9].x = random.randint(0, 1080)
    k[i + 9].op = rock


# Function to change x coordinate of obstacles

def change_x():
    for i in range(17):
        k[i].x = random.randint(0, 1080)


change_x()

# Initialising time to cross

cro1 = 0
cro2 = 0

# Starting loop

loop = True

# Creating Players

p1 = Player(screen_x / 2, screen_y - 50 + 10, 20, 20)
p2 = Player(screen_x / 2, 0 + 20, 20, 20)
while loop:

    # Framerate = 60

    clock.tick(60)

    # Assigning velocities to obstacles

    if which_p == 1:
        for i in range(9):
            k[i].ve = v1[i]
    else:
        for i in range(9):
            k[i].ve = v2[i]

    # Checking for crash

    if which_p == 1:
        for i in range(17):
            if cra != True:
                cra = crash(p1, k[i])
        if cra:
            p1.hit()
            youhit()
            cra = False
    else:
        for i in range(17):
            if cra != True:
                cra = crash(p2, k[i])
        if cra:
            p2.hit()
            youhit()
            cra = False

    # Switching players if one dies

    if p1.health <= 0:
        p1.health = -1
        which_p = 2
    if p2.health <= 0:
        p2.health = -1
        which_p = 1

    # Quitting game if quit button clicked

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            loop = False

    # Assigning controls to players

    keys = pygame.key.get_pressed()
    if which_p == 1:
        if keys[pygame.K_UP] and p1.y > p1.ve + p1.height:
            p1.y = p1.y - p1.ve
        if keys[pygame.K_DOWN] and p1.y < screen_y - 30:
            p1.y = p1.y + p1.ve
        if keys[pygame.K_RIGHT] and p1.x < screen_x - p1.width - p1.ve:
            p1.x = p1.x + p1.ve
        if keys[pygame.K_LEFT] and p1.x > p1.ve:
            p1.x = p1.x - p1.ve
    else:
        if keys[pygame.K_w] and p2.y > p2.ve + p2.height:
            p2.y = p2.y - p2.ve
        if keys[pygame.K_s] and p2.y < screen_y - p2.ve:
            p2.y = p2.y + p2.ve
        if keys[pygame.K_d] and p2.x < screen_x - p2.width - p2.ve:
            p2.x = p2.x + p2.ve
        if keys[pygame.K_a] and p2.x > p2.ve:
            p2.x = p2.x - p2.ve

    # Increasing score when cross obstacles

    if which_p == 1:
        if p1.y + 20 < 620 and ispass[0] == 0:
            p1.score = p1.score + 10
            ispass[0] = 1
        if p1.y + 20 < 570 and ispass[1] == 0:
            p1.score = p1.score + 5
            ispass[1] = 1
        if p1.y + 20 < 500 and ispass[2] == 0:
            p1.score = p1.score + 20
            ispass[2] = 1
        if p1.y + 20 < 450 and ispass[3] == 0:
            p1.score = p1.score + 10
            ispass[3] = 1
        if p1.y + 20 < 400 and ispass[4] == 0:
            p1.score = p1.score + 20
            ispass[4] = 1
        if p1.y + 20 < 350 and ispass[5] == 0:
            p1.score = p1.score + 10
            ispass[5] = 1
        if p1.y + 20 < 280 and ispass[6] == 0:
            p1.score = p1.score + 10
            ispass[6] = 1
        if p1.y + 20 < 230 and ispass[7] == 0:
            p1.score = p1.score + 5
            ispass[7] = 1
        if p1.y + 20 < 180 and ispass[8] == 0:
            p1.score = p1.score + 20
            ispass[8] = 1
        if p1.y + 20 < 130 and ispass[9] == 0:
            p1.score = p1.score + 10
            ispass[9] = 1
        if p1.y + 20 < 70 and ispass[10] == 0:
            p1.score = p1.score + 10
            ispass[10] = 1
    else:
        if p2.y > 620 - 30 and ispass2[0] == 0:
            p2.score = p2.score + 5
            ispass2[0] = 1
        if p2.y > 570 - 30 and ispass2[1] == 0:
            p2.score = p2.score + 20
            ispass2[1] = 1
        if p2.y > 500 - 30 and ispass2[2] == 0:
            p2.score = p2.score + 10
            ispass2[2] = 1
        if p2.y > 450 - 30 and ispass2[3] == 0:
            p2.score = p2.score + 20
            ispass2[3] = 1
        if p2.y > 400 - 30 and ispass2[4] == 0:
            p2.score = p2.score + 10
            ispass2[4] = 1
        if p2.y > 350 - 30 and ispass2[5] == 0:
            p2.score = p2.score + 10
            ispass2[5] = 1
        if p2.y > 280 - 20 and ispass2[6] == 0:
            p2.score = p2.score + 5
            ispass2[6] = 1
        if p2.y > 230 - 30 and ispass2[7] == 0:
            p2.score = p2.score + 20
            ispass2[7] = 1
        if p2.y > 180 - 30 and ispass2[8] == 0:
            p2.score = p2.score + 10
            ispass2[8] = 1
        if p2.y > 130 - 30 and ispass2[9] == 0:
            p2.score = p2.score + 10
            ispass2[9] = 1
        if p2.y > 655 - 30 and ispass2[10] == 0:
            p2.score = p2.score + 10
            ispass2[10] = 1

    # Score due to time taken to cross

    if which_p == 1:
        if p1.y + 20 < 620 and cro1 == 0:
            start = pygame.time.get_ticks()
            cro1 = 1
        if p1.y + 20 < 70:
            ttime1 = (pygame.time.get_ticks() - start) // 1000
    else:
        if p2.y > 100 and cro2 == 0:
            star = pygame.time.get_ticks()
            cro2 = 1
        if p2.y > 625:
            ttime2 = (pygame.time.get_ticks() - star) // 1000

    # When player1 crosses the river

    if p1.y < 35:

        # Reset player 1 and switch to player 2

        p1.x = screen_x / 2
        p1.y = screen_y - 40
        which_p = 2
        reach()

        # Resetting achievements

        for i in range(11):
            ispass[i] = 0
        cro1 = 0

        # Assigning score

        p1.score = p1.score + 300 // ttime1

        # Increasing velocity of obstacles

        for i in range(9):
            v1[i] = v1[i] + ee[i]

        # Case when player2 is already dead

        if p2.health <= 0:
            level = level + 1
            change_x()
            pygame.time.delay(100)

    # When player2 crosses the river

    if p2.y > 660:
        p2.x = screen_x / 2
        p2.y = 20
        which_p = 1
        reach()
        for i in range(11):
            ispass2[i] = 0
        cro2 = 0
        p2.score = p2.score + 300 // ttime2
        for i in range(9):
            v2[i] = v2[i] + ee[i]

        # Level increases

        level = level + 1
        change_x()
        pygame.time.delay(100)

    # Flipping triangles when players switch

    if which_p == 1:
        imm = triangle
    else:
        imm = opptri

    # Changing position of obstacles after level

    ran()
    refresh()

pygame.quit()
