import pygame
pygame.init()
screen_x=1080
screen_y=720
clock=pygame.time.Clock()
win=pygame.display.set_mode((screen_x, screen_y))
pygame.display.set_caption("Game")
font = pygame.font.SysFont('freesans', 16, True)
def youhit():
    win.fill((0,0,0))
    dis_mes=font.render("You have hit an obstacle",1,(255,255,255))
    win.blit(dis_mes,(510,340))
    pygame.display.update()
    pygame.time.delay(1000)
def reach():
    win.fill((0,0,0))
    dis=font.render("You have reached the end", 1, (255,255,255))
    win.blit(dis,(510,340))
    pygame.display.update()
    pygame.time.delay(1000) 
